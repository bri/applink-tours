import {Routes} from '@angular/router';
import {Error404Component} from './errors/404/error.404.component';
import {EventsListComponent,
  EventDetailComponent,
  CreateEventComponent,
  EventRouteActivator,
  EventListResolverService,
  CreateSessionComponent
 } from './events/index';


export const appRoutes=[
    {
        path:'events/new',
        component:CreateEventComponent
    },
    {
        path:'events',
        component :EventsListComponent,
        resolve:{
            events:EventListResolverService
        }
    },
    {
        path:'events/session/new',
        component :CreateSessionComponent
    },
    {
        path:'events/:id',
        component :EventDetailComponent
        ,canActivate:[EventRouteActivator]
    },
    {
        path:'404',
        component :Error404Component
    },   
    {
        path:'',
        redirectTo:'/events', 
        pathMatch:'full'
    },
    {
        path:'user',
        loadChildren:'./user/user.module#UserModule'
    }
]