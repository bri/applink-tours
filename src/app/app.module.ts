import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AlertModule } from 'ng2-bootstrap/alert';
import { AppComponent } from './app.component';
import {RouterModule} from '@angular/router';
import {ToastrService} from './common/toastr.service';
import {appRoutes} from './app.routing';
import { Error404Component } from './errors/404/error.404.component'; 
import {DatepickerModule} from 'ng2-bootstrap/datepicker';
import {TimepickerModule } from 'ng2-bootstrap';
import { NgUploaderModule } from 'ngx-uploader';
//events
import {EventsListComponent,
  EventThumbnailComponent,
  EventService,
  EventDetailComponent,
  CreateEventComponent,
  EventRouteActivator,
  EventListResolverService,
  CreateSessionComponent,
  SessionListComponent
 } from './events/index';

 import {AuthService} from './user/services/auth.service';
import { CollapsibleWellComponent } from './common/collapsible-well/collapsible-well.component';
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';
const myFirebaseConfig = {
    apiKey: "AIzaSyCeQOtiyn8BhvUNX5a63htMEkZeyhQVWsA",
    authDomain: "applink-tour.firebaseapp.com",
    databaseURL: "https://applink-tour.firebaseio.com",
    storageBucket: "applink-tour.appspot.com",
    messagingSenderId: "616731416650"
  };

const myFirebaseAuthConfig = {
  provider: AuthProviders.Google,
  method: AuthMethods.Redirect
};

@NgModule({
  declarations: [
    AppComponent,
    EventsListComponent,
    EventThumbnailComponent,
    EventDetailComponent,
    CreateEventComponent,
    Error404Component,
    CreateSessionComponent,
    SessionListComponent,
    CollapsibleWellComponent 
  ],
  imports: [
     AlertModule.forRoot(),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(myFirebaseConfig, myFirebaseAuthConfig),
    DatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    NgUploaderModule
  ],
  providers: [EventService,
  ToastrService,
  EventRouteActivator,
  EventListResolverService,
  AuthService 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
