import { Component, OnInit } from '@angular/core';
import {EventService} from '../shared/event.service';
import {ActivatedRoute} from '@angular/router';
import {IEvent,ISession} from '../index';
import {Observable} from 'rxjs/RX';
@Component({
  selector: 'event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.css']
})
export class EventDetailComponent implements OnInit {
  addMode:boolean;
  isDataReady:boolean=false;
  constructor(private _eventService:EventService,
  private route:ActivatedRoute
  ) { }
  event:IEvent;
  ngOnInit() {
  
 this._eventService.getEvent(this.route.snapshot.params['id']).subscribe(x=>{
      this.event=x;
      console.log("route snapshot", this.event.name);
      this.isDataReady=true;

    });

    
     
  }
  addSession(){
    this.addMode=true;
  }

  // saveNewSession(session:ISession){
  //   const nextId= Math.max.apply(null,this.event.sessions.map(
  //     x=> x.id
  //   ));
  //   session.id= nextId+1;
  //   this.event.sessions.push(session);
  //   this._eventService.updateEvent(this.event);
  //   this.addMode=false;
  // }

  // cancelAddSession(){
  // console.log("cancel")
  //   this.addMode=false;
  // }
 
}
