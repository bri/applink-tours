import { Component, OnInit } from '@angular/core';
import {EventService} from '../shared/event.service';
import {ToastrService} from '../../common/toastr.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.css']
})
export class EventsListComponent implements OnInit {

events:any;

  constructor(private _eventService:EventService,
  private _toastrService:ToastrService,
  private route:ActivatedRoute
  ) { }

  ngOnInit() {
  //  this.events =this.route.snapshot.data['events'];
      console.log("getting tours..");
      this._eventService.getTours().subscribe( x=>{
      this.events = x;
      //console.log("events : ", this.events);
      });

}

  handleEventClicked(data){
    console.log('received:',data);
  }
 handleThumbnailClick(eventName){
  this._toastrService.success(eventName);
 } 
}
