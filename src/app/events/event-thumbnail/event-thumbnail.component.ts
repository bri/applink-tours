import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import {IEvent} from '../index';

@Component({
  selector: 'event-thumbnail',
  templateUrl: './event-thumbnail.component.html'
})
export class EventThumbnailComponent implements OnInit {
  @Input() event:any;
  @Output() eventClick = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  clickMe(){
   this.eventClick.emit(this.event.name);
  }
// pag more than 50% pa ang remaining slots. green.
// more than 10% and less than 50% warning
// less than or equal sa 10%. danger
  getSlotsDisplay(totalSlots:number, remainingSlots:number){
    if(remainingSlots>= totalSlots/2)
      return 'label-success';
    else if (remainingSlots<= totalSlots*.4 && remainingSlots>totalSlots*.1)
      return 'label-warning';
    else
    return 'label-danger';
    }




}
