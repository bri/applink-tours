import { Component, OnInit,NgZone,Inject,EventEmitter,ElementRef } from '@angular/core';
import {Router} from '@angular/router';
import {EventService} from '../shared/event.service';
import * as firebase from 'firebase';
import { NgUploaderOptions, UploadedFile, UploadRejected } from 'ngx-uploader';


@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})

export class CreateEventComponent implements OnInit {

options: NgUploaderOptions;
  response: any;
  hasBaseDropZoneOver: boolean;
 myImage:any;
 dateToday:Date;
 downloadUrl:string;
 errorMessage: string;
 sizeLimit: number = 1000000; // 1MB
 inputUploadEvents: EventEmitter<string>;
  previewData: any;
 // isDirty:boolean=true;
  constructor(private router:Router,
  private _eventService:EventService
  ,@Inject(NgZone) private zone: NgZone
  ,private el: ElementRef
  ) { }
 
  ngOnInit() {
    this.dateToday = new Date();
    //this.today();
    this.options = new NgUploaderOptions({
      url: 'http://api.ngx-uploader.com/upload',
      filterExtensions: true,
      allowedExtensions: ['jpg', 'png','PNG','jpeg'],
      data: { userId: 12 },
      autoUpload: false,
      fieldName: 'file',
      fieldReset: true,
      maxUploads: 2,
      method: 'POST',
      previewUrl: true,
      withCredentials: false
    });

    this.inputUploadEvents = new EventEmitter<string>();

 
  }
  today(){
    console.log("calls today", new Date());
    return new Date();
  }

  saveEvent(formValues){
    //this._eventService.saveEvent(formValues);
   //  this.isDirty=false;
   //  this._eventService.addItem("test");
   if(!formValues.date)
      formValues.date= new Date();
    this._eventService.addTour(formValues);
      this.router.navigate(['/events']); 
    
  }
  cancel(){
    this.router.navigate(['/events']); 
  } 
  uploadFile(){
    console.log("uploading..,.",this.myImage);
  }

   handleUpload(data: any) {
    setTimeout(() => {
      this.zone.run(() => {
        this.response = data;
        if (data && data.response) {
            this.response = JSON.parse(data.response);
            console.log("data : ..", this.response);
       this.onUpload(); 
  //  this.myImage=this.response[0].originalname;
  //from yt
  //  const storageRef = firebase.storage().ref().child(this.response[0].filename);
  //   storageRef.getDownloadURL().then(url => this.myImage);

  // Create a reference to 'images/mountains.jpg'
  //var mountainImagesRef = storageRef.child(this.response[0].path);

  // mountainsRef.put(this.response[0].filename).then(()=>{
  //    console.log('Uploaded a blob or file!');

  // });        
        }
      });
    });
  }

  fileOverBase(e: boolean) {
    this.hasBaseDropZoneOver = e;
  }
  // upload() {
  //       let inputEl: HTMLInputElement = this.inputEl.nativeElement;
  //       let fileCount: number = inputEl.files.length;
  //       let formData = new FormData();
  //       if (fileCount > 0) { // a file was selected
  //           for (let i = 0; i < fileCount; i++) {
  //               formData.append('file[]', inputEl.files.item(i));
  //           }
  //           this.http
  //               .post('http://your.upload.url', formData)
  //               // do whatever you do...
  //               // subscribe to observable to listen for response
  //       }
  //   }
  file: File;
  onChange(event: EventTarget) {
        let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
        let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
        let files: FileList = target.files;
        this.file = files[0];
    }
  startUpload() {
    this.inputUploadEvents.emit('startUpload');
  }

  handlePreviewData(data: any) {
    this.previewData = data;
  }
  onUpload(){
    //   var storageRef = firebase.storage().ref();
    //   var mountainsRef = storageRef.child('images/'+this.response[0].filename); 
    //   mountainsRef.put(this.file).then((x)=>{
    //  console.log('Uploaded!');
    //  console.log("value of return data :", x);
    //  });      
  }
  beforeUpload(uploadingFile: UploadedFile): void {
    if (uploadingFile.size > this.sizeLimit) {
      uploadingFile.setAbort();
      this.errorMessage = 'File is too large!';
    }
  }




}