import { Injectable } from '@angular/core';
import {Router,RouterStateSnapshot,CanActivate,ActivatedRouteSnapshot} from '@angular/router';
import {EventService} from './event.service';
import {Observable } from 'rxjs/Observable';

@Injectable()
export class EventRouteActivator implements CanActivate {
  constructor(private _eventService:EventService,
        private router:Router) { }

canActivate(route:ActivatedRouteSnapshot):Observable<boolean>{
// this._eventService.getEvent(route.params['id']).subscribe(x=>{
//   console.log("value of exists", x.$value);
//    if(!x.$value)
//       this.router.navigate(['/404']);
//     return x;
// });
// return true;
return this._eventService.getEvent(route.params['id']).map(x=>{
  console.log("value of exists", x);
   if(!x.name)
      this.router.navigate(['/404']);
   return true;
}).catch(()=>{
   this.router.navigate(['/404']);
   return Observable.of(false);
});

}



    // canActivate(next:ActivatedRouteSnapshot, state:RouterStateSnapshot) {
    //     return this.loginService.isLoggedIn().map(e => {
    //         if (e) {
    //             return true;
    //         }
    //     }).catch(() => {
    //         this.router.navigate(['/login']);
    //         return Observable.of(false);
    //     });
    // }   



}
