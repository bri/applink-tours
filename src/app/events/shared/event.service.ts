import { Injectable,OnInit } from '@angular/core';
import {Http} from '@angular/http';
import {Subject,Observable} from 'rxjs/RX';
import {IEvent} from './event.model';
import { AngularFire,FirebaseListObservable,FirebaseApp } from 'angularfire2';
import * as firebase from 'firebase';
const EVENTS:IEvent[] =[];


@Injectable()
export class EventService implements OnInit {
   items: FirebaseListObservable<IEvent[]>;
image:string;
  constructor(private _http:Http,private af: AngularFire) {

    const storageRef = firebase.storage().ref().child('path/image.png');
    console.log("ref", storageRef);
    // storageRef.getDownloadURL().then(url => this.image);

   }

  ngOnInit(){
 
}
getTours(){
 this.items = this.af.database.list('/tours') as FirebaseListObservable<IEvent[]>;
    return this.items;  
}
addItem(newName: string) {
    this.items.push({ name: newName,
          date:newName,
          time:newName,
          price:newName,
          location:newName,
          totalSlot:newName,
          remainingSlot:newName
         });
  }

getEvents():Subject<IEvent[]>{
  let subject = new Subject<IEvent[]>();
  setTimeout(function() {
    subject.next(EVENTS);
    subject.complete();
  }, 100);

  return subject;
}
getEvent(id:string){
 // return this.items.find(x=> x.$key===id);
let event = this.af.database.object("/tours/"+id);
// as FirebaseListObservable<any>;
  return event;
}
addTour(formValue){

  let newTour = {
    name: formValue.name,
    date: formValue.date.toString(),
    location:formValue.location,
    price:formValue.price,
    slots:formValue.slots,
    slotsRemaining:formValue.slots,
    time:formValue.time
  };

  console.log('adding tours..',newTour);
 // return this.items.push(newTour);
  
}
saveEvent(event){
    event.id = 999;
    event.session=[];
    EVENTS.push(event);
}

 updateEvent(event){
    let index= EVENTS.findIndex(x=>x.$key= event.id)
    EVENTS[index] = event;
  }


}
