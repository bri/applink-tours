export interface IEvent{
    $key?:string
    name:string,
    date:Date,
    time:string,
    price:number,
    slots:number,
    slotsRemaining:number,
    image:string,
    location?:{
        address:string,
        city:string,
        country:string
    },
    onlineUrl?:string
    sessions?:ISession[]
}
export interface ISession{
    id:number
    name:string
    presenter:string
    duration:number
    level:string
    voters:string[]
}