import { Injectable } from '@angular/core';
import {Resolve} from '@angular/router';
import {EventService} from './event.service';
@Injectable()
export class EventListResolverService  implements Resolve<any>{

  constructor(private _es:EventService) { }
  resolve(){
    return this._es.getEvents().map(x=> x);
  }
}
