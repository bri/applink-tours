import {RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {userRoutes} from './user.routing';
import {ProfileComponent} from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { SignUpComponent } from './sign-up/sign-up.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(userRoutes),
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [],
    declarations: [
        ProfileComponent,
        LoginComponent,
        SignUpComponent
    ],
    providers: [],
})
export class UserModule { }




