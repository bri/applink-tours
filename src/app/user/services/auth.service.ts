import { Injectable,OnInit } from '@angular/core';
import {IUser} from '../shared/user.model';
import {Router} from '@angular/router';
import { AngularFire,AuthProviders,AuthMethods } from 'angularfire2';
import {Subject,Observable} from 'rxjs/Rx';
@Injectable()
export class AuthService implements OnInit {
 public isLoggedIn:boolean=false;
  currentUser:IUser=<IUser>{};
  constructor(public af: AngularFire,
    private router:Router
  
  ) { }

 ngOnInit(){
   //this.af.auth.subscribe(auth => console.log(auth.auth.));
}
  loginUser(userName:string,password:string){
    this.currentUser={
      id:1,
      userName:"bri",
      firstName:"bri",
      lastName:"asdf"
    };
  }
  isAuthenticated(){
   this.af.auth.subscribe(auth =>{
   
     if(auth){
         this.currentUser.firstName=auth.auth.displayName;
         this.currentUser.userName=auth.auth.email;
         
       return this.isLoggedIn=true;
     }
       return  this.isLoggedIn=false;
   })

   this.af.auth.subscribe
  //  return !!this.currentUser;
  }

  updateCurrentUser(firstName:string,lastName:string){
    this.currentUser.firstName=firstName;
    this.currentUser.lastName= lastName;
  }

  login(userName:string,password:string) {
    this.af.auth.login({ email: userName, password:password },{
  provider: AuthProviders.Password,
  method: AuthMethods.Password,
  }).then(()=>{

       console.log("logging in...");
       this.router.navigate(['events']);
    })
    .catch( error => console.log(error.message));
   
  }

  logout() {
     this.af.auth.logout();
  }

  createUser(username:string,password:string){
    var creds = {email:username,password:password};
   this.af.auth.createUser(creds).then(()=>{
    this.af.auth.login(creds);
    console.log("logging in...");
   }).catch( ()=>{
    console.log("error register user..");
   })



  }


}
