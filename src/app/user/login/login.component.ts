import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loading:boolean=false;
  constructor(private _authService:AuthService,
    private router:Router) { }

  ngOnInit() {
  }
login(formValues){
  console.log("form values",formValues);
  
  this._authService.login(formValues.userName,formValues.password);

  //this.router.navigate(['events']);
}
  cancel(){
 this.router.navigate(['events']);
  }
  toRegister(){
    this.router.navigate(['user/register']);
  }
}
