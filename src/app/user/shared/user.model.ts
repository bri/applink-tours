export interface IUser{
    id:number,
    firstName:string,
    lastName:string,
    userName:string
}

export interface ISession{
    id:number;
    name:string;  
    voters:string[];    
}