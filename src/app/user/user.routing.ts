import {ProfileComponent} from './profile/profile.component';
import {Routes} from '@angular/router';
import { LoginComponent } from './login/login.component';
import {SignUpComponent} from './sign-up/sign-up.component';
export const userRoutes =[
    {
        path:'profile',
        component:ProfileComponent
    },
    {
        path:'login',
        component:LoginComponent    
    },
    {
         path:'register',
        component:SignUpComponent
    }
]