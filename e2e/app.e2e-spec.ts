import { EventMgtPage } from './app.po';

describe('event-mgt App', function() {
  let page: EventMgtPage;

  beforeEach(() => {
    page = new EventMgtPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
